package com.matthewlp.department.service;

import com.matthewlp.department.entity.Department;
import com.matthewlp.department.repo.DepRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class DepService {
    @Autowired
    private DepRepo depRepo;

    public Department saveDepartment(Department department) {

        return depRepo.save(department);
    }

    public Department findDepByid(Long depId) {

        return depRepo.findByDepId(depId);
    }
}
