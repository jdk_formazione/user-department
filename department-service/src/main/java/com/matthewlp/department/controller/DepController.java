package com.matthewlp.department.controller;

import com.matthewlp.department.entity.Department;
import com.matthewlp.department.service.DepService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/deps")
@Slf4j
public class DepController {
    @Autowired
    private DepService depService;
    @PostMapping("/")
    public Department saveDepartment(@RequestBody Department department){

        return depService.saveDepartment(department);
    }
    @GetMapping("/{id}")
    public Department findDepById(@PathVariable("id") Long depId){
        return depService.findDepByid(depId);
    }
}
